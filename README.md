# YouTube Anonymous\* Playlist Generator
A shell script utilizing a simple url hack in order to generate 
YouTube playlists from lists of urls without signing up/in. 

## Usage
Create playlists by providing a `.txt` files with YouTube urls (one url per
line, see examples in `./lists/`).
Run the script with an arbitrary amount of lists as argument(s). The script
will generate playlists accordingly.

### Example
Input:
``` bash
./playlist ./lists/*
```
Output:
``` bash
[./lists/example.txt]
https://www.youtube.com/watch_videos?video_ids=J---aiyznGQ,ZZ5LpwO-An4
```
